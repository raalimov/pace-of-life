#### Установка зависимостей

    virtualenv venv -p /usr/bin/python3 --no-site-package
    source venv/bin/activate
    pip install -r requirements.txt

#### Старт прокси сервера

    python src/manage.py runserver 127.0.0.1:8000

#### Запуск тестов
    cd src/
    pytest