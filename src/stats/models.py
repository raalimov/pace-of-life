from django.db import models

from django.contrib.auth import get_user_model


class TimeStampedModel(models.Model):
    time_start = models.TimeField('Time start')
    time_end = models.TimeField('Time end')

    class Meta:
        abstract = True


class Sleep(TimeStampedModel):
    user = models.ForeignKey(get_user_model(), verbose_name='User')


class Steps(TimeStampedModel):
    user = models.ForeignKey(get_user_model(), verbose_name='User')
    value = models.PositiveIntegerField('Value')


class Location(TimeStampedModel):
    user = models.ForeignKey(get_user_model(), verbose_name='User')
    latitude = models.FloatField('Latitude')
    longitude = models.FloatField('Longitude')

