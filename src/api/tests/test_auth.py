import pytest


@pytest.mark.django_db
def test_get_token_ok(client, token, auth_data):
    response = client.post('/api/token-auth/',
                           data={'username': auth_data['username'],
                                 'password': auth_data['password']})
    assert response.status_code == 200, response.data
    assert 'token' in response.data
    assert token.key == response.data['token']