import pytest

from django.contrib.auth import get_user_model
from django.shortcuts import reverse
from rest_framework import status
from mixer.backend.django import mixer

from stats.models import Sleep, Steps, Location

User = get_user_model()


@pytest.fixture
def sleeps(user):
    return mixer.cycle(5).blend(
        Sleep, user=user, time_start='15:00:00', time_end='20:00:00'
    )


@pytest.fixture
def sleeps2(user):
    return mixer.cycle(2).blend(
        Sleep, user=user, time_start='20:00:00', time_end='7:00:00'
    )


@pytest.fixture
def steps(user):
    return mixer.cycle(5).blend(
        Steps, user=user, time_start='15:00:00', time_end='20:00:00'
    )


@pytest.fixture
def steps2(user):
    return mixer.cycle(2).blend(
        Steps, user=user, time_start='20:00:00', time_end='7:00:00'
    )


@pytest.fixture
def locations(user):
    return mixer.cycle(5).blend(
        Location, user=user, time_start='15:00:00', time_end='20:00:00'
    )


@pytest.fixture
def locations2(user):
    return mixer.cycle(2).blend(
        Location, user=user, time_start='20:00:00', time_end='7:00:00'
    )


@pytest.mark.django_db
def test_get_sleeps(client, headers, sleeps):
    response = client.get(reverse('api:sleeps-list'), **headers)
    assert response.status_code == status.HTTP_200_OK
    assert len(sleeps) == len(response.json())


@pytest.mark.django_db
def test_filter_sleeps(client, headers, sleeps, sleeps2):
    response = client.get(
        reverse('api:sleeps-list'),
        data={'time_start': '09:00:00', 'time_end': '19:00:00'}, **headers
    )
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == len(sleeps)

    response = client.get(
        reverse('api:sleeps-list'),
        data={'time_start': '19:00:00', 'time_end': '6:00:00'}, **headers
    )
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == len(sleeps2)


@pytest.mark.django_db
def test_get_steps(client, headers, steps):
    response = client.get(reverse('api:steps-list'), **headers)
    assert response.status_code == status.HTTP_200_OK
    assert len(steps) == len(response.json())


@pytest.mark.django_db
def test_filter_steps(client, headers, steps, steps2):
    response = client.get(
        reverse('api:steps-list'),
        data={'time_start': '09:00:00', 'time_end': '19:00:00'}, **headers
    )
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == len(steps)

    response = client.get(
        reverse('api:steps-list'),
        data={'time_start': '19:00:00', 'time_end': '6:00:00'}, **headers
    )
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == len(steps2)


@pytest.mark.django_db
def test_get_locations(client, headers, locations):
    response = client.get(reverse('api:locations-list'), **headers)
    assert response.status_code == status.HTTP_200_OK
    assert len(locations) == len(response.json())


@pytest.mark.django_db
def test_filter_locations(client, headers, locations, locations2):
    response = client.get(
        reverse('api:locations-list'),
        data={'time_start': '09:00:00', 'time_end': '19:00:00'}, **headers
    )
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == len(locations)

    response = client.get(
        reverse('api:locations-list'),
        data={'time_start': '19:00:00', 'time_end': '6:00:00'}, **headers
    )
    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == len(locations2)
