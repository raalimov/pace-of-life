import time

from django.db.models import Q
from rest_framework.filters import BaseFilterBackend


class TimeFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        query = None
        str_time_start = request.GET.get('time_start')
        str_time_end = request.GET.get('time_end')
        time_start = self._str_to_time(request.GET.get('time_start'))
        time_end = self._str_to_time(request.GET.get('time_end'))

        if time_start and time_end:
            query = Q(**{'time_start__gte': str_time_start})
            if time_start < time_end:
                query &= Q(**{'time_start__lte': str_time_end})
            else:
                query |= Q(**{'time_start__lte': str_time_end})
        elif time_start:
            query = Q(**{'time_start__gte': str_time_start})
        elif time_end:
            query = Q(**{'time_start__lte': str_time_end})

        if query:
            queryset = queryset.filter(query)

        return queryset

    def _str_to_time(self, value):
        try:
            timestamp = time.mktime(time.strptime(value, "%H:%M:%S"))
        except (TypeError, ValueError):
            return None
        else:
            return timestamp


class IsOwnerFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(user=request.user)

