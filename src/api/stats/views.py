from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from api.filters import IsOwnerFilterBackend, TimeFilterBackend


from api.stats.serializers import (
    SleepSerializer, StepsSerializer, LocationSerializer
)
from stats.models import Sleep, Steps, Location


class SleepViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = SleepSerializer
    queryset = Sleep.objects.all()
    filter_backends = (IsOwnerFilterBackend, TimeFilterBackend)


class StepsViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = StepsSerializer
    queryset = Steps.objects.all()
    filter_backends = (IsOwnerFilterBackend, TimeFilterBackend)


class LocationViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = LocationSerializer
    queryset = Location.objects.all()
    filter_backends = (IsOwnerFilterBackend, TimeFilterBackend)