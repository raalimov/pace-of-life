from rest_framework import serializers

from stats.models import Sleep, Steps, Location


class SleepSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sleep
        fields = ('time_start', 'time_end')


class StepsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Steps
        fields = ('time_start', 'time_end', 'value')


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('time_start', 'time_end', 'latitude', 'longitude')
