from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.authtoken import views

from api.stats.views import SleepViewSet, StepsViewSet, LocationViewSet

router = routers.DefaultRouter()
router.register(r'sleeps', SleepViewSet, base_name='sleeps')
router.register(r'steps', StepsViewSet, base_name='steps')
router.register(r'locations', LocationViewSet, base_name='locations')


urlpatterns = [
    url(r'^token-auth/', views.obtain_auth_token),
    url(r'^', include(router.urls))
]

