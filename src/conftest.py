import pytest

from django.contrib.auth import get_user_model
from mixer.backend.django import mixer
from rest_framework.authtoken.models import Token


User = get_user_model()


@pytest.fixture
def auth_data():
    return {'username': 'test_username', 'password': 'test_password'}


@pytest.fixture
def user(auth_data):
    user = mixer.blend(User, username=auth_data['username'])
    user.set_password(auth_data['password'])
    user.save()
    return user


@pytest.fixture
def token(user):
    return mixer.blend(Token, user=user)


@pytest.fixture
def headers(token):
    auth_headers = {
        'HTTP_AUTHORIZATION': 'Token ' + token.key,
    }
    return auth_headers


@pytest.fixture(scope='session')
def django_db_setup():
    from django.conf import settings
